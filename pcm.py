from flask.ext.api import FlaskAPI
from flask.ext.api import status

from flask import request

from voluptuous import MultipleInvalid
from voluptuous import Required
from voluptuous import Schema


app = FlaskAPI(__name__)


COURSES = []


@app.route('/courses', methods=['GET', 'POST'])
def courses():
    if request.method == 'POST':
        schema = Schema({
            Required('title'): str,
            Required('slug'): str,
            Required('description'): str,
        })
        try:
            course = schema(request.data)
        except MultipleInvalid as e:
            return {'detail': str(e)}, status.HTTP_400_BAD_REQUEST
        COURSES.append(course)
        return {'course': course}, status.HTTP_201_CREATED
    return {'courses': COURSES}


@app.route('/courses/<str:slug>', methods=['GET'])
def course():
    pass


if __name__ == "__main__":
    app.run(debug=True)
